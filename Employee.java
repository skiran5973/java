/**
* Sriram Satyavolu  © Threerhinos LLC.
**/
import java.util.Date;

public class Employee {
	private Date datehired;
	private int id;
	private String name;
	
	public Employee(Date datehired, int id, String name) {
		super();
		this.datehired = datehired;
		this.id = id;
		this.name = name;
	}

	public Date getDatehired() {
		return datehired;
	}

	public void setDatehired(Date datehired) {
		this.datehired = datehired;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	} 
	
	public boolean isManager() {
		return this instanceof Manager;
	}
	public boolean isPartTimeEmployee() {
		return this instanceof PartTimeEmployee;
	}
	
	public static void main(String[] args) {
		Employee employee = new Employee(new Date(), 1001, "SriramEmployee" );
		Manager manager = new Manager (new Date(), 1002, "SriramManager" );
		PartTimeEmployee partTimeEmployee = new PartTimeEmployee (new Date(), 1003, "PartTimeEmployee" );
		
		System.out.println("Employee is manager ? " + employee.isManager() );
		System.out.println("Employee is PartTimeEmployee? " + employee.isPartTimeEmployee() );
		
		System.out.println("Manager is manager ? " + manager.isManager() );
		System.out.println("Manager is PartTimeEmployee? " + manager.isPartTimeEmployee() );

		System.out.println("PartTimeEmployee is manager ? " + partTimeEmployee.isManager() );
		System.out.println("PartTimeEmployee is PartTimeEmployee? " + partTimeEmployee.isPartTimeEmployee() );

	}
	
}

class Manager extends Employee {

	public Manager(Date datehired, int id, String name) {
		super(datehired, id, name);
	}
	
}

class PartTimeEmployee extends Employee{

	public PartTimeEmployee(Date datehired, int id, String name) {
		super(datehired, id, name);
	}
	
}